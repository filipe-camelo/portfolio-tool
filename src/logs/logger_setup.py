# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 22:46:25 2017

@author: fvcam
"""

import os
import json
import logging.config

"""
Setup logging.
"""
def setup_logging(
    default_path='finance_test_logger.json',
    default_level=logging.INFO,
    env_key='LOG_CFG'
):
    """
    Setup logging configuration
    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)