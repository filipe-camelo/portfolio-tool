# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 22:20:46 2017

@author: Filipe Camelo
"""

from classes.data.data_classes import IntrinioGetter
from classes.data.data_classes import DbHandler

from logs.logger_setup import setup_logging


ip_db = '192.168.1.116'
db_name = 'finance_data'

# Start the logger
setup_logging()

ig = IntrinioGetter()

# Get data from NASDAQ
df = ig.getExchangeData('^XNAS')
# get dta from NYSE
df = ig.getExchangeData('^XNYS')

# create the database interaction class
db = DbHandler(db_name)

# new line
