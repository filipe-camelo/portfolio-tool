# -*- coding: utf-8 -*-
"""
Created on Mon Apr 10 16:46:11 2017

@author: Filipe Camelo
"""

from sqlalchemy import create_engine
import quandl as qd
import pandas as pd
import logging
import requests as rq
from pandas.io.json import json_normalize

logger = logging.getLogger(__name__)

"""
Class QuandlGetter 

Handles the data collection from the Quandl database.
"""


class QuandlGetter:
    """
    Constructor receives a database to retrieve from.
    """

    def __init__(self, _db='WIKI'):
        self.api_key = 'HYPnwBg9KfAuF3KGcxbU'
        self.db = _db

    """
    Get a table from the Qandl dtabase from a specified period.

    tik: ticker symbol
    per_s: start of period to retrieve
    per_e: end of the period to retrieve

    returns: a table with the data from the tiker and period specified
    """

    def getSecurityData(self, tik, per_s=None, per_e=None):
        try:
            logger.info('Getting data from ticker %s', tik)
            res = qd.get(self.getDb() + '/' + tik, start_date=per_s, end_date=per_e)
            logger.info(tik + " retrieval OK")
            return res
        except Exception:
            logger.error('Data retrival for %s failed', tik, exc_info=True)
            return None

    """
    Get adjusted close for multiple tickers

    tiks: arrays of strings with the tickers
    per_s: start of period to retrieve
    per_e: end of the period to retrieve

    returns: a DataFrame with date as index with each ticker being as the
    name of each column
    """

    def getMultTicks(self, tiks, per_s=None, per_e=None):
        ac = 'Adj. Close'

        # get table
        tbs = list()
        for item in tiks:
            tbs.append(self.getSecurityData(item, per_s, per_e).
                       rename(columns={ac: item}))

        # transform columns to be adjusted close of each ticker
        adj_tb = pd.DataFrame(tbs[0][tiks[0]])
        for item in range(len(tiks) - 1):
            item = item + 1
            aux = pd.DataFrame(tbs[item][tiks[item]])
            adj_tb = pd.merge(adj_tb, aux, left_index=True, right_index=True)

        return adj_tb

    """
    Getters and setters
    """
    """
    Returns the variable db from the instance.
    """

    def getDb(self):
        return self.db


"""
Class DbHandler

Database handler class including all the methods required to access, retrieve
and write to the database. Creates a standerdized way to access the database.
"""


class DbHandler:
    def __init__(self, _db, _host='localhost'):
        self.hostname = _host
        self.dbname = _db
        self.engine = self.createEngine()

    """
    Create an engine to the database for use with pandas

    returns: cursor to execute queries.
    """

    def createEngine(self):
        conn_str = "postgresql://dbuser:postgres@" + self.getHostname() + ":5432/" + self.getDbname()
        # conn.cursor will return a cursor object, you can use this cursor to perform queries
        try:
            # get a connection, if a connect cannot be made an exception will be raised here
            eng = create_engine(conn_str)
            logger.info("Postgres engine created successfully")
            return eng
        except Exception:
            logger.error("Unbale to connect to Database", exc_info=True)
            return None

    """
    Selects a table from a database and a schema.

    _tb: table name
    _sch: schema name, leave empty for public schema

    returns: returns a DataFrame type object with the intire table.
    """

    def selectTable(self, _tb, _sch=None):
        try:
            logger.info('Reading table %s', _tb)
            res = pd.read_sql_table(_tb, self.getEngine(), schema=_sch)
            logger.info('Table %s read success', _tb)
            return res
        except Exception:
            logger.error('Unable to get table %s', _tb, exc_info=True)
            return None

    """
    Insert a table from a DataFrame object directly to a specified 
    table and schema.

    returns: True if succcessful and False if it fails.
    """

    def insertTable(self, _tb, _df, _sch=None):
        try:
            logger.info('Inserting table %s', _tb)
            _df.to_sql(_tb, self.getEngine(), index=False, schema=_sch,
                       chunksize=100, if_exists='append')
            logger.info('Insert table %s successfully', _tb)
            return True
        except Exception:
            logger.error('Insert table %s failed', _tb)
            return False

    """
    Execute a user defined query to the database.

    _stament: SQL statement

    returns: result from the query, None if exception is raised.
    """

    def doQuery(self, _stament):
        try:
            logger.info('Executing query statement: %s', _stament)
            res = pd.read_sql_query(_stament, self.getEngine())
            logger.info('Query execution successful')
            return res
        except Exception:
            logger.error('Query failed')
            return False

    """
    Getters and setters
    """

    """
    Get the engine variable of the instance.
    """

    def getEngine(self):
        return self.engine

    """
    Get the hostname of the database
    """

    def getHostname(self):
        return self.hostname

    """
    Get the databse name
    """

    def getDbname(self):
        return self.dbname


"""
Intrinio class to get data from the intrinio web API using 
query strings.

"""

class IntrinioGetter:
    def __init__(self):
        self.usr = "b1b694bd75016a4a546dfaf3f18193fa"
        self.pss = "7e5abdb5d09bb3be99767a003e6c0f8b"
        self.url = "https://api.intrinio.com/"

    """
    Get information from the listed exchange companies.

    _exch: echange symbol to include in the API query

    returns: a DataFrame with the information received.
    """

    def getExchangeData(self, _exch):
        req = self.getUrl() + 'securities'
        logger.info('Getting page 1 of exchange data for:' + _exch)
        params = {'exch_symbol': _exch, 'us_only': 'Yes', 'page_number': '1'}
        resp = rq.get(req, params=params, auth=(self.getUsr(), self.getPss()))
        res = resp.json()
        df_res = json_normalize(res['data'])
        list_df = list()
        list_df.append(df_res)
        for item in range(2, res['total_pages'] + 1):
            logger.info('Gatting page ' + str(item))
            params = {'exch_symbol': _exch, 'us_only': 'Yes', 'page_number': str(item)}
            resp = rq.get(req, params=params, auth=(self.getUsr(), self.getPss()))
            res = resp.json()
            list_df.append(json_normalize(res['data']))
        return pd.concat(list_df, ignore_index=True)

    def getUrl(self):
        return self.url

    def getUsr(self):
        return self.usr

    def getPss(self):
        return self.pss
